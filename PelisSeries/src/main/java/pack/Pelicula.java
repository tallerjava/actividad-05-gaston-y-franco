package pack;

public class Pelicula extends Filmografia{
    
    public Pelicula(String titulo, String genero, int anio){
        super(titulo, genero, anio);
    }
    
    @Override
    public String toString(){
        return getEspectadores() + " Espectadores /// Titulo: " + titulo + " /// Genero: " + genero + " /// Año: " + anio;
    }
}