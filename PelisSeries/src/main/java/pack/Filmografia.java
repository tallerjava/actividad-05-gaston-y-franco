package pack;

import java.util.Random;

public class Filmografia {
    
    protected String titulo, genero;
    protected int anio, espectadores;
    
    public Filmografia(String titulo, String genero, int anio){
        this.titulo = titulo;
        this.genero = genero;
        this.anio = anio;
        Random randomint = new Random();
        espectadores = randomint.nextInt(10000)+150;
    }
    
    public String getTitulo(){
        return titulo;
    }
    
    public String getGenero(){
        return genero;
    }
    
    public int getAnio(){
        return anio;
    }
    
    public int getEspectadores(){
        return espectadores;
    }
}
