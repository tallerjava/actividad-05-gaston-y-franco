package pack;

public class Serie extends Filmografia{
    
    int temporada;

    public Serie(String titulo, String genero, int anio, int temporada){
        super(titulo, genero, anio);
        this.temporada = temporada;
    }
    
    public int getTemporada(){
        return temporada;
    }
    
    @Override
    public String toString(){
        return getEspectadores() + " Espectadores /// Serie: " + titulo + " /// " + genero + " /// Año: " + anio + " /// " + temporada + " temporadas";
    }
}        
