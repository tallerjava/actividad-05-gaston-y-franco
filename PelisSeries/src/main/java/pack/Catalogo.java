package pack;

import java.util.ArrayList;

public class Catalogo{
    
    private ArrayList<Filmografia>  listado = new ArrayList();

    public static void main(String[] args){
        Catalogo catalogo = new Catalogo();
        catalogo.cargarCatalogo();
        catalogo.mostrarMenosTaquillera();
        catalogo.mostrarMasTaquillera();
        catalogo.mostrarCatalogo();
    }
    
    private void cargarCatalogo(){
        listado.add(new Pelicula("Avangers: Infinity War","Acción",2018));
        listado.add(new Pelicula("Wind River","Drama",2017));
        listado.add(new Serie("Friends","Sitcom",1994,10));
        listado.add(new Serie("Scrubs","Comedia",2001,6));
        listado.add(new Pelicula("The Truman Show","Drama",1998));
        listado.add(new Pelicula("Top Secret!","Comedia",1984));
        listado.add(new Serie("Breaking Bad","Drama",2008,7));
    }
    
    private void mostrarCatalogo(){
        listado.forEach(System.out::println);
    }
    
    private Filmografia getMenosTaquillera(){
        Filmografia filmografia_min = listado.get(0);
        for(Filmografia film : listado){
            if(film.espectadores <= filmografia_min.espectadores){
                filmografia_min = film;
            }
        }
        return filmografia_min;
    }
    
    private Filmografia getMasTaquillera(){
        Filmografia filmografia_max = listado.get(0);
        for(Filmografia film : listado){
            if(film.espectadores >= filmografia_max.espectadores){
                filmografia_max = film;
            }   
        }
        return filmografia_max;
    }
    
    private void mostrarMenosTaquillera(){
        System.out.println("--------\nMenos taquillera: " + getMenosTaquillera().titulo + " (" + getMenosTaquillera().espectadores + " espectadores)");
    }
    
    private void mostrarMasTaquillera(){
        System.out.println("Más taquillera: " + getMasTaquillera().titulo + " (" + getMasTaquillera().espectadores + " espectadores)\n--------");
    }
}